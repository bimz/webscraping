package bima.test.webscraping.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bima.test.webscraping.pojo.Product;

public class WebscrapingUtil {
	
	private static Logger log = LogManager.getLogger(WebscrapingUtil.class);
	
	private WebscrapingUtil() {
	}

	public static void webscraping() {
		log.info("WebScrapping Start");
		log.info("");

		int page = 1;
		int itemCount=1;
		int productNum = 1;
		List<Product> list = new ArrayList<>();

		try {
			while (itemCount <= productNum) {
				Document doc = Jsoup.connect("https://www.tokopedia.com/p/handphone-tablet/handphone?page=" + page).get();
				log.info("======================================================");
				log.info("Fetch page no. {} from {}", page, (doc.title() != null) ? doc.title() : "");
				log.info("======================================================");
				page++;
				Elements elements = doc.select("a[class*=css-89jnbj]");
				for (Element element : elements) {
					if (itemCount <= productNum) {
						Product product = getProductData(element.attr("href"));
						product.setMerchantName(getMerchantName(element));
						
						list.add(product);
						itemCount++;
					}
				}
			}
		} catch (IOException e) {
			log.error(e);
		} finally {
			log.info("");
			log.info("============");
			log.info("List Product");
			log.info("============");
			log.info(JsonUtil.objectToJson(list));
		}

		// Export to CSV
		ExportToCsv.exportListProduct(list);
		
		log.info("");
		log.info("WebScrapping End");
	}
	
	private static String getMerchantName(Element element) {
		Elements divMerchantElements = element.select("div[class*=css-vbihp9]");
		if (!divMerchantElements.isEmpty()) {
			Element divMerchantElement = divMerchantElements.get(0);
			Elements merchantElements = divMerchantElement.select("span");
			if (!merchantElements.isEmpty()) {
				Element merchantElement = merchantElements.get(1);
				return merchantElement.text();
			}
		}
		
		return "";
	}
	
	private static Product getProductData(String url) {
		Product result = new Product();
		try {
			log.info(url);
			Document productDoc = Jsoup.connect(url).get();
			/* There are some tokopedia pages that can't be accessed via JSOUP */
			
			// Product Name
			Elements productNameElements = productDoc.select("h1[data-testid*=lblPDPDetailProductName]");
			if (!productNameElements.isEmpty()) {
				Element productNameElement = productNameElements.get(0);
				result.setProductName(productNameElement.text());
			}
			
			// Description
			Elements descriptionElements = productDoc.select("div[data-testid*=lblPDPDescriptionProduk]");
			if (!descriptionElements.isEmpty()) {
				Element descriptionElement = descriptionElements.get(0);
				result.setDescription(descriptionElement.text());
			}
			
			// Image Link
			Elements divImageElements = productDoc.select("div[class*=css-1y5a13]");
			if (!divImageElements.isEmpty()) {
				Element divImageElement = divImageElements.get(0);
				Elements imageElements = divImageElement.select("img[crossorigin*=anonymous]");
				if (!imageElements.isEmpty()) {
					Element imageElement = imageElements.get(0);
					result.setImageLink(imageElement.attr("src"));
				}
			}
			
			// Price
			Elements priceElements = productDoc.select("div[data-testid*=lblPDPDetailProductPrice]");
			if (!priceElements.isEmpty()) {
				Element priceElement = priceElements.get(0);
				result.setPrice(priceElement.text());
			}
			
			// Rating
			// Can't get data via scraping
			
			// Merchant Name
			// Can't get data via scraping
		} catch (IOException e) {
			log.error(e);
		}
		
		return result;
	}

}
