package bima.test.webscraping.util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import bima.test.webscraping.pojo.Product;

public class ExportToCsv {

	private static Logger log = LogManager.getLogger(ExportToCsv.class);
	
	private static final String SEPARATOR = "|"; 

	private ExportToCsv() {
	}

	public static void exportListProduct(List<Product> list) {
		log.info("Saving to CSV Start");
		
		try (PrintWriter writer = new PrintWriter("product.csv")) {
			StringBuilder sb = new StringBuilder();

			// CSV separator config
			sb.append("sep=" + SEPARATOR);
			sb.append('\n');

			// CSV Header
			sb.append("Product Name");
			sb.append(SEPARATOR);
			sb.append("Description");
			sb.append(SEPARATOR);
			sb.append("Image Link");
			sb.append(SEPARATOR);
			sb.append("Price");
			sb.append(SEPARATOR);
			sb.append("Rating");
			sb.append(SEPARATOR);
			sb.append("Merchant Name");
			sb.append('\n');

			// CSV Body
			for (Product product : list) {
				sb.append(product.getProductName());
				sb.append(SEPARATOR);
				sb.append(product.getDescription());
				sb.append(SEPARATOR);
				sb.append(product.getImageLink());
				sb.append(SEPARATOR);
				sb.append(product.getPrice());
				sb.append(SEPARATOR);
				sb.append(product.getRating());
				sb.append(SEPARATOR);
				sb.append(product.getMerchantName());
				sb.append('\n');
			}

			writer.write(sb.toString());
			log.info("Saving to CSV Start");
		} catch (FileNotFoundException e) {
			log.error(e);
		}
	}

}
