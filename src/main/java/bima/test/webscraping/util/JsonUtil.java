package bima.test.webscraping.util;

import com.google.gson.Gson;

public class JsonUtil {

	private JsonUtil() {
	}

	public static String objectToJson(Object data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}
	
}
