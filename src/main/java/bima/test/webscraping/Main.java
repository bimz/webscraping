package bima.test.webscraping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;

import bima.test.webscraping.util.WebscrapingUtil;

@SpringBootConfiguration
public class Main {
	
	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
		WebscrapingUtil.webscraping();
	}
	
}
